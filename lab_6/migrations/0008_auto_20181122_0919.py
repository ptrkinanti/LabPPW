# Generated by Django 2.1.1 on 2018-11-22 02:19

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_6', '0007_auto_20181122_0155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 22, 2, 19, 31, 867794, tzinfo=utc)),
        ),
    ]

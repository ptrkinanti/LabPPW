# Generated by Django 2.1.1 on 2018-10-10 13:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=300)),
                ('time', models.TimeField(auto_now_add=True)),
            ],
        ),
    ]

from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}

def home(request):
    form = Status_Form(request.POST or None)
    allstatus = Status.objects.all()
    response = {
        "allstatus" : allstatus
    }
    response['form']=form

    if(request.method == "POST" and form.is_valid()):
        status = request.POST.get("status")
        Status.objects.create(status=status)
        return redirect('home')
    else:
        return render(request, 'home.html', response)

def profile(request):
    return render(request, 'profile.html', response)
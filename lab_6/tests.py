from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from .models import Status
from .views import home
from .views import profile
from .forms import Status_Form
import unittest

# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab_6/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_to_do_list_template(self):
        response = Client().get('/lab_6/')
        self.assertTemplateUsed(response, 'home.html')

    def test_lab6_using_create_form_func(self):
        found= resolve('/lab_6/')
        self.assertEqual(found.func, home)

    def test_lab_6_html_text(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<p class="h1">Hello, apa kabar?</p>', html_response)
    
    def test_model_can_create_new_status(self):
        new = Status.objects.create(status="currently stressed out doing this lab")
        
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)

    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab_6/profile')
        self.assertEqual(response.status_code, 200)

    def test_personal_text(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<div class="h1" align="left">personal</div>', html_response)
        
    
    def test_description_text(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('capricorn girl', html_response)

    def test_picture(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('personalpic.png', html_response)

class Lab6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Lab6FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium  = self.selenium
        selenium.get('https://ppw-d-ptrkinanti-lab4.herokuapp.com/lab_6')
        time.sleep(2)
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')
        status.send_keys('test')
        time.sleep(2)
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('test', selenium.page_source)

    def test_css_style_submit_button_in_webpage(self):
        selenium = self.selenium
        selenium.get('https://ppw-d-ptrkinanti-lab4.herokuapp.com/lab_6')
        button = selenium.find_element_by_tag_name('button')
        self.assertIn("submit", button.get_attribute("type"))

    def test_css_style_mode_button_in_webpage(self):
        selenium = self.selenium
        selenium.get('https://ppw-d-ptrkinanti-lab4.herokuapp.com/lab_6')
        button = selenium.find_element_by_id('change')
        self.assertIn("btn", button.get_attribute("class"))
    
    def test_css_style_button_in_profile(self):
        selenium = self.selenium
        selenium.get('https://ppw-d-ptrkinanti-lab4.herokuapp.com/lab_6')
        button = selenium.find_element_by_tag_name('button')
        self.assertIn("btn", button.get_attribute("class"))
    
    def test_title_in_landing_page(self):
        selenium = self.selenium
        selenium.get('https://ppw-d-ptrkinanti-lab4.herokuapp.com/lab_6')
        self.assertIn("landing page", selenium.title)

    def test_title_in_profile(self):
        selenium = self.selenium
        selenium.get('https://ppw-d-ptrkinanti-lab4.herokuapp.com/lab_6/profile')
        self.assertIn("welcome!", selenium.title)

    
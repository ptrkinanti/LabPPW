from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth import views
from .views import books
from .views import index
from .views import add
from .views import substract
from .views import getFavorite

app_name = 'lab_9'
urlpatterns = [
    path('', index, name = 'index'),
    path('books/', books, name = 'books'),
    path('add/', add, name="add"), 
    path('substract/', substract, name="substract"),
    path('getFavorite/', getFavorite, name="getFavorite"),
]
import unittest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from .views import index
from .views import books
from .views import add
from .views import substract
from .views import getFavorite

# Create your tests here.
class Lab9UnitTest(TestCase):
    def test_lab_9_url_exist(self):
        response = Client().get('/lab_9/')
        self.assertEqual(response.status_code, 200)

    def test_json_url_exist(self):
        response = Client().get('/lab_9/books')
        self.assertEqual(response.status_code, 301)

    def test_add_url_exist(self):
        response = Client().get('/lab_9/add')
        self.assertEqual(response.status_code, 301)
    
    def test_substract_url_exist(self):
        response = Client().get('/lab_9/substract')
        self.assertEqual(response.status_code, 301)

    def test_check_fav_url_exist(self):
        response = Client().get('/lab_9/getFavorite')
        self.assertEqual(response.status_code, 301)

    def test_lab_9_using_template(self):
        response = Client().get('/lab_9/')
        self.assertTemplateUsed(response, 'books.html')


class Lab9FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options )
        super(Lab9FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab9FunctionalTest, self).tearDown()

    def test_title(self):
        selenium = self.selenium
        selenium.get('https://ppw-d-ptrkinanti-lab4.herokuapp.com/lab_9')
        self.assertIn("books books books", selenium.title)
    
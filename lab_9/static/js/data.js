$(document).ready(function() {
	checkFavorite();
	$.ajax({
		url: "books",
		datatype: 'json',
		success: function(books){
			var obj = jQuery.parseJSON(books)
			renderHTML(obj);
		}
	})
});

var container = document.getElementById("content");

function renderHTML(books) {
	library = "<tbody>";
	for (i = 0; i < books.items.length; i++) {
		library += "<tr>" +
			"<th scope='row' class='center align-middle'>" + (i + 1) + "</th>" +
			"<td><img class='img-fluid' style='width:20vh' src='" + books.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.title + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.authors + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.publisher + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.publishedDate + "</td>" +
			"<td class='center align-middle'>" + "<img id='star" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
	}
	container.insertAdjacentHTML('beforeend', library + "</tbody>")
	checkFavorite();
}

var counter = 0;
function checkFavorite(){
    $.ajax({
        type: 'GET',
        url: '/lab_9/getFavorite/',
        datatype : 'json',
        success: function(data){
            for(var i=1; i <= data.message.length; i++){
                console.log(data.message[i-1]);
                var id = data.message[i-1];
                var td = document.getElementById(id);
                if(td!=null){
                    td.className = 'clicked';
                    td.src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
                }
                $('#counter').html(data.message.length);
            }
        }

    });
};

function favorite(id){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var user = document.getElementById(id);
    var added = 'https://image.flaticon.com/icons/svg/291/291205.svg';
    var substracted = 'https://image.flaticon.com/icons/svg/149/149222.svg';
    if(user.className == 'checked'){
        $.ajax({
            url: "/lab_9/substract/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                counter = result.message;
                user.className = '';
                user.src = substracted;
                $('#counter').html(counter);
            },
            error: function(errorMessage){
                alert("Something is wrong");
            }
        });
    }else{
        $.ajax({
            url: "/lab_9/add/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                console.log(user);
                counter = result.message;
                user.className = 'checked';
                user.src = added;
                $('#counter').html(counter);
            },
            error: function(errorMessage){
                alert("Something is wrong like");
            }
        })
    }
}
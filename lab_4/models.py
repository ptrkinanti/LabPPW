from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Schedule(models.Model):
    day = models.CharField(max_length=15)
    date = models.DateTimeField()
    time = models.TimeField()
    eventname = models.CharField(max_length=30)
    place = models.CharField(max_length=50)
    category = models.CharField(max_length=30)

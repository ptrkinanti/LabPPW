from django import forms
from .models import Schedule

class Schedule_Form(forms.Form):
    day = forms.CharField(label='day', max_length=15, required=True)
    date = forms.DateTimeField(label='date', required=True, 
                            widget=forms.DateTimeInput(attrs={'type':'date'}))
    time = forms.TimeField(label='time', required=True,
                            widget=forms.TimeInput(attrs={'type':'time'}))
    eventname = forms.CharField(label='event name', max_length=30, required=True)
    place = forms.CharField(label='place', max_length=50, required=True)
    category = forms.CharField(label='category', max_length=30, required=True)
from django.shortcuts import render
from django.http import HttpResponse
from .forms import Schedule_Form
from .models import Schedule
from django.shortcuts import redirect

response={}
def main(request):
	
	return render(request,'main.html',response)

def business(request):
	
	return render(request,'business.html',response)

def personal(request):
	
	return render(request,'personal.html',response)

def short(request):
	return render(request,'short.html',response)

def contact(request):
	return render(request,'contact.html',response)

def guestbook(request):
	return render(request,'guestbook.html',response)

def createschedule(request):
	schedules = Schedule.objects.all()
	response = {
		"schedules" : schedules
	}
	response['sched'] = Schedule_Form
	return render(request, 'createschedule.html', response)

def schedule(request):
	form = Schedule_Form(request.POST or None)
	if(request.method == 'POST'):
		if(form.is_valid()):
			response['eventname'] = request.POST.get("eventname")
			response['day'] = request.POST.get("day")
			response['date'] = request.POST.get("date")
			response['time'] = request.POST.get("time")
			response['place'] = request.POST.get("place")
			response['category'] = request.POST.get("category")
			sched = Schedule(day=response['day'], date=response['date'], 
							time=response['time'], eventname=response['eventname'],
							place=response['place'], category=response['category'])
			sched.save()
			schedules = Schedule.objects.all()
			response['schedules']= schedules
			return render(request, 'schedule.html', response)
		else:
			return render(request, 'createschedule.html', response)
	else:
		response['form'] = form
		return render(request, 'schedule.html', response)

def delete(request):
	schedules = Schedule.objects.all().delete()
	response['schedules'] = schedules
	return render(request, 'schedule.html', response)
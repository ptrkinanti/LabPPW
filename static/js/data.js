$(document).ready(function() {
	$.ajax({
		url: "books",
		datatype: 'json',
		success: function(books){
			var obj = jQuery.parseJSON(books)
			renderHTML(obj);
		}
	})
});

var container = document.getElementById("content");

function renderHTML(books) {
	library = "<tbody>";
	for (i = 0; i < books.items.length; i++) {
		library += "<tr>" +
			"<th scope='row' class='center align-middle'>" + (i + 1) + "</th>" +
			"<td><img class='img-fluid' style='width:20vh' src='" + books.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.title + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.authors + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.publisher + "</td>" +
			"<td class='center align-middle'>" + books.items[i].volumeInfo.publishedDate + "</td>" +
			"<td class='center align-middle'>" + "<img id='star" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
	}
	container.insertAdjacentHTML('beforeend', library + "</tbody>")
}

var counter = 0;

function favorite(clicked_id) {
	var star = document.getElementById(clicked_id);
	if (star.classList.contains("check")) {
		star.classList.remove("check");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;
	} else {
		star.classList.add('check');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/616/616489.png';
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;
	}
}
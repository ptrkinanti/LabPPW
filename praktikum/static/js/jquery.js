$(function(){
    $("#accordion").accordion(
    {   collapsible: true, 
        active: false
    });
} );

$(function() {
    $( document ).tooltip();
  } );

$(function(){
    var invertMode = localStorage.getItem('invertMode') || true;
    $("#change").click(
         
        function(){
            $("body").fadeIn("3000");
            $(".box").fadeIn("3000");
            if(invertMode){
                $(this).css({"color" : "#FFFFFF"});
                $(this).css({"background-color" : "#121212"});
                $(this).text("normal mode")
                $("body").css({"backgroundColor" : "#121212"});
                $("body").css({"color" : "#FFFFFF"}); 
                $(".box").css({"backgroundColor" : "#121212"});
                $(".box").css({"color" : "#FFFFFF"}); 
                localStorage.setItem('invertMode',true);
                invertMode=false;
            }
            else{
                $(this).css({"color" : "#121212"});
                $(this).css({"background-color" : "#FFFFFF"});
                $(this).text("invert mode")
                $("body").css({"backgroundColor" : "#FFFFFF"});
                $("body").css({"color" : "#121212"}); 
                $(".box").css({"backgroundColor" : "#FFFFFF"});
                $(".box").css({"color" : "#121212"}); 
               localStorage.setItem('invertMode',false);
                invertMode=true;
            }
    });
});

$("#buttonjson").click(
    function(){
        $.ajax({
            url: 
        "http://localhost:8000/static/JSON/data.json",
            success: function(result){
                $.each(result, function(i, field){
                    $("#demo").append(field + " ");
                });
            }
        });
});




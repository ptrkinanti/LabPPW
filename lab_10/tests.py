from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from django.db import IntegrityError
from .views import subscribe, validation
from .models import Subscriber
from .forms import FormSubscriber
import unittest

# Create your tests here.
class Lab_10_Test(TestCase):
    def test_lab_10_url_is_exist(self):
        response = Client().get('/lab_10/')
        self.assertEqual(response.status_code, 200)

    def test_lab_10_using_subscribe_template(self):
        response = Client().get('/lab_10/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_lab_10_using_subscribe_func(self):
        found = resolve('/lab_10/')
        self.assertEqual(found.func, subscribe)

    def test_lab_10_using_checkEmail_func(self):
        found = resolve('/lab_10/validation/')
        self.assertEqual(found.func, validation)

    def test_model_can_create_new_subscriber(self):
        new_subscriber = Subscriber.objects.create(name="aku", email="aku@gmail.com", password="halo22hehe")
        count = Subscriber.objects.all().count()
        self.assertEqual(count, 1)

    def test_FormSubscriber_valid(self):
        form = FormSubscriber(data={'name': "aku", 'email': "aku@gmail.com" , 'password': "memememe2121"})
        self.assertTrue(form.is_valid())
    
    def test_max_length_name(self):
        name = Subscriber.objects.create(name="maximum of 30 char")
        self.assertLessEqual(len(str(name)), 30)
    
    def test_unique_email(self):
        Subscriber.objects.create(email="aku@gmail.com")
        with self.assertRaises(IntegrityError):
            Subscriber.objects.create(email="aku@gmail.com")
        
    def test_check_email_view_get_return_200(self):
        email = "aku@gmail.com"
        Client().post('/lab_10/validation/', {'email': email})
        response = Client().post('/lab_10/', {'email': 'aku@gmail.com'})
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        Subscriber.objects.create(name="aku", email="aku@gmail.com", password="hahahahahah")
        response = Client().post('/lab_10/validation/', {
            "email": "aku@gmail.com"
        })
        self.assertEqual(response.json()['is_email'], True)

    def test_subscribe_should_return_status_subscribe_true(self):
        response = Client().post('/lab_10/', {
            "name": "aku",
            "email": "aku@gmail.com",
            "password":  "hahahahahah",
        })
        self.assertEqual(response.json()['status_subscribe'], True)

    def test_subscribe_should_return_status_subscribe_false(self):
        name, email, password = "aku", "aku@gmail.com", "hahahahaha"
        Subscriber.objects.create(name=name, email=email, password=password)
        response = Client().post('/lab_10/', {
            "name": name,
            "email": email,
            "password":  password,
        })
        self.assertEqual(response.json()['status_subscribe'], False)

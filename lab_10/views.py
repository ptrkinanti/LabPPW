from django.http import JsonResponse
from django.shortcuts import render, redirect
from .models import Subscriber
from .forms import FormSubscriber
from django.views.decorators.csrf import csrf_exempt

response = {}
def subscribe(request):
    if request.method == 'POST':
        form = FormSubscriber(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status_subscribe = True
            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status_subscribe = False
            return JsonResponse({'status_subscribe' : status_subscribe})
    Form = FormSubscriber()
    response['form'] = Form
    return render(request, 'subscribe.html', response)

@csrf_exempt 
def validation(request):
    if request.method == 'POST':
        email = request.POST['email']
        validasi = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email' : validasi})

def listOfSubscribers(request):
    allsubscribers = Subscriber.objects.all().values()
    subs = list(allsubscribers)
    return JsonResponse({'allsubscribers' : subs})

@csrf_exempt
def unsubscribe(request):
    if request.method == 'POST':
        email = request.POST['email']
        Subscriber.objects.get(email=email).delete()
        return redirect('subscribe')
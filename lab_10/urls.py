from django.contrib import admin
from django.urls import path
from . import views

app_name = 'lab_10'
urlpatterns = [
    path('', views.subscribe, name='subscribe'),
    path('validation/', views.validation, name="validation"),
    path('list/', views.listOfSubscribers, name='listOfSubscribers'),
    path('unsubscribe/', views.unsubscribe, name='unsubscribe'),
]

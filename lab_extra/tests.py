import unittest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from .views import log
from .views import books


# Create your tests here.
class LabExtraUnitTest(TestCase):
    def test_lab_extra_url_exist(self):
        response = Client().get('/lab_extra/')
        self.assertEqual(response.status_code, 200)

    def test_json_url_exist(self):
        response = Client().get('/lab_extra/books/')
        self.assertEqual(response.status_code, 200)

    def test_html_templates(self):
        request = HttpRequest()
        response = log(request)
        html_response = response.content.decode("utf8")
        self.assertIn("Best Selling Books", html_response)

    def test_lab_extra_using_to_do_list_template(self):
        response = Client().get('/lab_extra/')
        self.assertTemplateUsed(response, 'login.html')

    def test_lab_extra_using_log_func(self):
        found = resolve('/lab_extra/')
        self.assertEqual(found.func, log)

    def test_lab_extra_using_books_func(self):
        found = resolve('/lab_extra/books/')
        self.assertEqual(found.func, books)

from django.apps import AppConfig


class LabExtraConfig(AppConfig):
    name = 'lab_extra'

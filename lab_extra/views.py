from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
import requests
import json

response={}
def log(request):
	return render(request,'login.html',response)

def books(request):
    read = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    parse = json.dumps(read)
    return HttpResponse(parse)
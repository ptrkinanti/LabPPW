from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth import views
from .views import books
from .views import log

urlpatterns = [
    path('', log, name = 'log'),
    path('books/', books, name = 'books'),
    path('login/',  views.LoginView.as_view(), name="login"),
    path('logout/', views.LogoutView.as_view(),  {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]